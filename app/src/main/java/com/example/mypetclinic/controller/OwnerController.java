package com.example.mypetclinic.controller;

import com.example.mypetclinic.services.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class OwnerController {

    private final OwnerService ownerService;
}
