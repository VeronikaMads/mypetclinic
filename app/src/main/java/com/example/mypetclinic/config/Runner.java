package com.example.mypetclinic.config;

import com.example.mypetclinic.entity.*;
import com.example.mypetclinic.repository.*;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Component("MainRunner")
public class Runner implements CommandLineRunner {

    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final VisitRepository visitRepository;
    private final NotesRepository notesRepository;
    private final ProductRepository productRepository;
    private final ItemRepository itemRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;
    @Override
    public void run(String... args) throws Exception {
//        encryption();
//        createVisit();
//        createProduct();
//        createReceipt();
        initData();//инициализирует стандарный набор данных
        createUser();


    }

    private void createUser() {
        Permissions perm = permissionRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);
        Filial filial= Filial.builder()
                .filialName("filial1")
                .city("Minsk")
                .build();

        User user1 = User.builder()
                .login("user1")
                .password("1234")
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .role(Role.ADMIN)
                .build();
        User user2 = User.builder()
                .login("user2")
                .password("5678")
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .role(Role.MANAGER)
                .build();
        userRepository.saveAll(Arrays.asList(user1,user2));

    }

    private void initData() {
        Permissions permissions = Permissions.builder()
                .name("update_user")
                .build();
        Permissions permissions2 = Permissions.builder()
                .name("delete_user")
                .build();
        Permissions permissions3 = Permissions.builder()
                .name("create_visit")
                .build();
        Permissions permissions4 = Permissions.builder()
                .name("get_pet")
                .build();
        permissionRepository.saveAll(Arrays.asList(permissions,permissions2,permissions3,permissions4));
    }

    private void createReceipt() {
        Receipt receipt = Receipt.builder()
                .amout(BigDecimal.TEN)
                .build();
        Item item = Item.builder()
                .name("coffee")
                .price(BigDecimal.ONE)
                .receipt(receipt)
                .build();
        Item item2 = Item.builder()
                .name("pizza")
                .price(BigDecimal.TEN)
                .receipt(receipt)
                .build();
        receipt.setItems(List.of(item, item2));
        receiptRepository.save(receipt);


    }

    private void encryption() {
//        String pwd = "dev_pwd";
//        String encrypt = stringEncryptor.encrypt(dbPwd);
//        System.out.println(encrypt);
//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);
        System.out.println(dbPwd);

    }

    private void createVisit() {
        Notes notes = Notes.builder().description("descr1").build();
        Notes notes2 = Notes.builder().description("descr2").build();

        Visit visit = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes)
                .build();
        Visit visit2 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes2)
                .build();

        visitRepository.saveAll(Arrays.asList(visit, visit2));
        notes.setDescription("descr1_update");
        visit.setVersion(2);
        visitRepository.save(visit);

        visit2.setNotes(null);
        visitRepository.save(visit2);
    }

    private void createProduct() {
        Product product1 = Product.builder()
                .name("cola")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("001") //код группы
                        .codePart("abc") // код товара
                        .build())
                .build();
        Product product2 = Product.builder()
                .name("fanta")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("002") //код группы
                        .codePart("abc") // код товара
                        .build())
                .build();
        productRepository.save(product1);
        productRepository.save(product2);
    }
}
