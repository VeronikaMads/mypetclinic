package com.example.mypetclinic.services.jpa;

import com.example.mypetclinic.entity.Owner;
import com.example.mypetclinic.entity.Pet;
import com.example.mypetclinic.services.PetService;

import com.example.mypetclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@JpaImplementation
public class PetJpaServiceImpl extends AbstractJpaService<Pet, Long> implements PetService {


    @Override
    public JpaRepository<Pet, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Pet findByOwner(Owner owner) {
        throw new UnsupportedOperationException();
    }
}
