package com.example.mypetclinic.services.map;


import com.example.mypetclinic.entity.Owner;
import com.example.mypetclinic.entity.Pet;
import com.example.mypetclinic.services.PetService;
import com.example.mypetclinic.services.config.MapImplementation;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@MapImplementation
public class PetMapService extends AbstractMapService<Pet, Long> implements PetService {

    private static final Map<Long, Pet> resource = new HashMap<>();

    @Override
    public Map<Long, Pet> getResource() {
        return resource;
    }

    @Override
    public Pet findByOwner(Owner owner) {
        return null; // to do
    }
}
