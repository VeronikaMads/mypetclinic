package com.example.mypetclinic.services.map;


import com.example.mypetclinic.entity.Pet;
import com.example.mypetclinic.entity.Vet;
import com.example.mypetclinic.services.VetService;
import com.example.mypetclinic.services.config.MapImplementation;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class VetMapService extends AbstractMapService<Vet, Long> implements VetService {

    private static final Map<Long, Vet> resource = new HashMap<>();

    @Override
    public Map<Long, Vet> getResource() {
        return resource;
    }

    @Override
    public Collection<Vet> findBySpecification(String specification) {
        return null; // to do
    }
}
