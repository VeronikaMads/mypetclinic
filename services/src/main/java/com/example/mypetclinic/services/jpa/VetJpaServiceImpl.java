package com.example.mypetclinic.services.jpa;


import com.example.mypetclinic.entity.Vet;
import com.example.mypetclinic.services.VetService;
import com.example.mypetclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@JpaImplementation
public class VetJpaServiceImpl extends AbstractJpaService<Vet, Long> implements VetService {


    @Override
    public JpaRepository<Vet, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Vet> findBySpecification(String specification) {
        throw new UnsupportedOperationException();
    }
}
