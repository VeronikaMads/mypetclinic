package com.example.mypetclinic.services;

import com.example.mypetclinic.entity.Vet;

import java.util.Collection;

public interface VetService extends CrudService<Vet, Long> {

    Collection<Vet> findBySpecification(String specification);


}
