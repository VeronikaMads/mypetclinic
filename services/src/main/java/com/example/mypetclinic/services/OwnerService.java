package com.example.mypetclinic.services;

import com.example.mypetclinic.entity.Owner;

import java.util.Collection;

public interface OwnerService extends CrudService<Owner, Long> {


    Collection<Owner> findByName(String name);


}
