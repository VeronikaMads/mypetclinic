package com.example.mypetclinic.services;

import java.util.Map;

public interface BaseMapService<T, ID> {
    Map<ID, T> getResource();
}
