package com.example.mypetclinic.services.map;

import com.example.mypetclinic.entity.Owner;
import com.example.mypetclinic.services.OwnerService;
import com.example.mypetclinic.services.config.MapImplementation;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
@MapImplementation
public class OwnerMapService extends AbstractMapService<Owner, Long> implements OwnerService {

    private static final Map<Long, Owner> resource = new HashMap<>();

    @Override
    public Map<Long, Owner> getResource() {
        return resource;
    }

    @Override
    public Collection<Owner> findByName(String name) {
        return null; // to do
    }
}
