package com.example.mypetclinic.services.jpa;

import com.example.mypetclinic.entity.Owner;
import com.example.mypetclinic.services.OwnerService;
import com.example.mypetclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@JpaImplementation
public class OwnerJpaServiceImpl extends AbstractJpaService<Owner, Long> implements OwnerService {


    @Override
    public JpaRepository<Owner, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Owner> findByName(String name) {
        throw new UnsupportedOperationException();
    }
}
