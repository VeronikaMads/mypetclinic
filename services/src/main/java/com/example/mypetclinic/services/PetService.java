package com.example.mypetclinic.services;

import com.example.mypetclinic.entity.Owner;
import com.example.mypetclinic.entity.Pet;


public interface PetService extends CrudService<Pet, Long> {

    Pet findByOwner(Owner owner);
}
