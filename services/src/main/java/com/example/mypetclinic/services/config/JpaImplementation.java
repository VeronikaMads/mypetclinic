package com.example.mypetclinic.services.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Service
@ConditionalOnProperty(name = "spring.data.jpa.repositories.enabled", havingValue = "true")
public @interface JpaImplementation {
}
