package com.example.mypetclinic.entity;

import lombok.*;

import javax.persistence.*;
import java.lang.annotation.Target;
import java.time.OffsetDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "vet_visit")
public class Visit {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private int version;
    @Column(name = "visit_time")
    private OffsetDateTime time;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "visit_note_ID")
    private Notes notes;
}
