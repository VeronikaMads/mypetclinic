package com.example.mypetclinic.entity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
public class Pet implements BaseEntity<Long>{
    private Long id;
    private String name;
    private Owner owner;
    private LocalDate birthDate;
    public PetType petType;


}
