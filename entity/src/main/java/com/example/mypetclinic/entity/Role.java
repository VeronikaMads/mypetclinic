package com.example.mypetclinic.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.activation.UnsupportedDataTypeException;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum Role {
    ADMIN("Adm"), MANAGER("Mng"), USER("Usr");
    private final String value;
    private static final Map<String, Role> MAP = Arrays.stream(Role.values())
            .collect(Collectors.toMap(Role::getValue, Function.identity()));

    public static Role getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new RuntimeException();
        }
        Role role = MAP.get(value);
        return Objects.isNull(role) ? USER : role;
    }
}
