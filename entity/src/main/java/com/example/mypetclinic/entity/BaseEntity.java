package com.example.mypetclinic.entity;

public interface BaseEntity <ID>{
    ID getId();

}