package com.example.mypetclinic.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

//чек
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
public class Receipt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal amout;
    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL) // чек у многих элементов
    private List<Item> items;
}
