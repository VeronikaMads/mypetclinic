package com.example.mypetclinic.entity;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;
@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductPK implements Serializable {
    private String code;
    private String codePart;
}
