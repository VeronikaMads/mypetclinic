package com.example.mypetclinic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
public class Filial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String filialName;
    private String city;
    @ManyToMany(mappedBy = "filials")
    private Set<User> users;
}
