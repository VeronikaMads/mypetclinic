package com.example.mypetclinic.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PetType {
    CAT("Cat"),
    DOG("Dog");
    private final String value;


}
