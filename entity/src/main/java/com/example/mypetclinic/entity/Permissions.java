package com.example.mypetclinic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

//доступ/разрешение
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
public class Permissions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToMany(mappedBy = "permissions",cascade = {CascadeType.DETACH,CascadeType.REFRESH})
    private List<User> users;

}
