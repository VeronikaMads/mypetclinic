package com.example.mypetclinic.entity;

import com.example.mypetclinic.entity.converter.RoleConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    @Convert(converter = RoleConverter.class)
    private Role role;
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "user_perm",
            joinColumns = @JoinColumn(name = "usr_id"),
            inverseJoinColumns = @JoinColumn(name = "perm_id"))
    private Set<Permissions> permissions;
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Filial> filials;

}
