package com.example.mypetclinic;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Getter
@Setter
@ConfigurationProperties(prefix = "cache.ttl")
@Component
public class CacheProperties {
    private int pets;
    private int vets;
    private int owners;

}
