package com.example.mypetclinic.repository;

import com.example.mypetclinic.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item,Long> {
}
