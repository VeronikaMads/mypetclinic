package com.example.mypetclinic.repository;

import com.example.mypetclinic.entity.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permissions,Long> {

    Optional<Permissions> findByName(String name);
}
