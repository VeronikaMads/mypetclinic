package com.example.mypetclinic.repository;

import com.example.mypetclinic.entity.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Notes,Long> {
}
