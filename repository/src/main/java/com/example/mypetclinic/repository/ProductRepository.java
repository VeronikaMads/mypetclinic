package com.example.mypetclinic.repository;

import com.example.mypetclinic.entity.Product;
import com.example.mypetclinic.entity.ProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, ProductPK> {
}
